package com.epam.rd.java.basic.task8;

public class GrowingTips {
    private int temperature;
    private String lighting;
    private int watering;

    public GrowingTips(int temperature, String lighting, int watering) {
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
    }

    public GrowingTips() {
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getLighting() {
        return lighting;
    }

    public int getWatering() {
        return watering;
    }
}
