package com.epam.rd.java.basic.task8.controller;


import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.VisualParameters;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.*;



/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	private List<Flower> flowers;

	public List<Flower> getFlowers() {
		if(flowers == null){
			initializeFlowers();
		}
		return flowers;
	}

	private void initializeFlowers() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		flowers = new ArrayList<>();
		try {
			factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			DocumentBuilder db = factory.newDocumentBuilder();
			Document doc = db.parse(new File(xmlFileName));
			doc.getDocumentElement().normalize();
			NodeList list = doc.getElementsByTagName("flower");
			for(int i=0; i<list.getLength(); ++i){
				Node node = list.item(i);
				if(node.getNodeType() == Node.ELEMENT_NODE){
					Element element = (Element) node;
					String name = element.getElementsByTagName("name").item(0).getTextContent();
					String soil = element.getElementsByTagName("soil").item(0).getTextContent();
					String origin = element.getElementsByTagName("origin").item(0).getTextContent();

					Node param = element.getElementsByTagName("visualParameters").item(0);
					Element paramElement = (Element) param;
					String paramStemColour = paramElement.getElementsByTagName("stemColour").item(0).getTextContent();
					String paramLeafColour = paramElement.getElementsByTagName("leafColour").item(0).getTextContent();
					int paramAveLenFlower = Integer.parseInt(paramElement.getElementsByTagName("aveLenFlower").item(0).getTextContent());

					Node tips = element.getElementsByTagName("growingTips").item(0);
					Element tipsElement = (Element) tips;
					Node lightingNode =  tipsElement.getElementsByTagName("lighting").item(0);
					Element lightElement = (Element) lightingNode;
					String tipsLighting = lightElement.getAttribute("lightRequiring");
					int tipsWatering = Integer.parseInt(tipsElement.getElementsByTagName("watering").item(0).getTextContent());
					int tipsTemperature = Integer.parseInt(tipsElement.getElementsByTagName("temperature").item(0).getTextContent());

					String multiplying = element.getElementsByTagName("multiplying").item(0).getTextContent();
					flowers.add(new Flower(name, soil, origin, new VisualParameters(paramStemColour, paramLeafColour,
							paramAveLenFlower), new GrowingTips(tipsTemperature,tipsLighting,tipsWatering), multiplying));
				}
			}
		}catch(ParserConfigurationException | SAXException | IOException e){
			e.printStackTrace();
		}

	}

	public void createXMLFile (String outputXmlFile) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.newDocument();
			Element root = doc.createElement("flowers");
			Attr rootAttr1 = doc.createAttribute("xmlns");
			rootAttr1.setValue("http://www.nure.ua");
			root.setAttributeNode(rootAttr1);
			Attr rootAttr2 = doc.createAttribute("xmlns:xsi");
			rootAttr2.setValue("http://www.w3.org/2001/XMLSchema-instance");
			root.setAttributeNode(rootAttr2);
			Attr rootAttr3 = doc.createAttribute("xsi:schemaLocation");
			rootAttr3.setValue("http://www.nure.ua input.xsd");
			root.setAttributeNode(rootAttr3);
			doc.appendChild(root);

			for (Flower f:
				 flowers) {
				Element flower = doc.createElement("flower");
				root.appendChild(flower);

				Element name = doc.createElement("name");
				name.appendChild(doc.createTextNode(f.getName()));
				flower.appendChild(name);

				Element soil = doc.createElement("soil");
				soil.appendChild(doc.createTextNode(f.getSoil()));
				flower.appendChild(soil);

				Element origin = doc.createElement("origin");
				origin.appendChild(doc.createTextNode(f.getOrigin()));
				flower.appendChild(origin);

				Element param = doc.createElement("visualParameters");
				Element stemColour = doc.createElement("stemColour");
				stemColour.appendChild(doc.createTextNode(f.getParam().getStemColour()));
				param.appendChild(stemColour);
				Element leafColour = doc.createElement("leafColour");
				leafColour.appendChild(doc.createTextNode(f.getParam().getLeafColour()));
				param.appendChild(leafColour);
				Element aveLenFlower = doc.createElement("aveLenFlower");
				aveLenFlower.appendChild(doc.createTextNode(String.valueOf(f.getParam().getAveLenFlower())));
				param.appendChild(aveLenFlower);
				Attr lenAttr = doc.createAttribute("measure");
				lenAttr.setValue("cm");
				aveLenFlower.setAttributeNode(lenAttr);
				flower.appendChild(param);

				Element tips = doc.createElement("growingTips");
				Element temperature = doc.createElement("temperature");
				temperature.appendChild(doc.createTextNode(String.valueOf(f.getTips().getTemperature())));
				Attr tempAttr = doc.createAttribute("measure");
				tempAttr.setValue("celcius");
				temperature.setAttributeNode(tempAttr);
				tips.appendChild(temperature);
				Element lighting = doc.createElement("lighting");
				//lighting.appendChild(doc.createTextNode(f.getTips().getLighting()));
				tips.appendChild(lighting);
				Attr attr = doc.createAttribute("lightRequiring");
				attr.setValue(f.getTips().getLighting());
				lighting.setAttributeNode(attr);
				Element watering = doc.createElement("watering");
				watering.appendChild(doc.createTextNode(String.valueOf(f.getTips().getWatering())));
				Attr tempWater = doc.createAttribute("measure");
				tempWater.setValue("mlPerWeek");
				watering.setAttributeNode(tempWater);
				tips.appendChild(watering);
				flower.appendChild(tips);

				Element multiplying = doc.createElement("multiplying");
				multiplying.appendChild(doc.createTextNode(f.getMultiplying()));
				flower.appendChild(multiplying);
			}
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.setOutputProperty(OutputKeys.METHOD, "xml");
			t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			DOMSource domSource = new DOMSource(doc);
			StreamResult streamResult = new StreamResult(new File(outputXmlFile));
			t.transform(domSource, streamResult);

		} catch (ParserConfigurationException | TransformerException pce) {
			pce.printStackTrace();
		}
	}
}
