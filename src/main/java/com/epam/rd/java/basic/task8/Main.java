package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;


public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowers = new Flowers(domController.getFlowers());
		flowers.sortByName();
		String outputXmlFile = "output.dom.xml";
		domController.createXMLFile(outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSAX = new Flowers(saxController.getFlowers());
		//flowersSAX.sortByOrigin();
		outputXmlFile = "output.sax.xml";
		saxController.createXMLFile(outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSTAX = new Flowers(staxController.getFlowers());
		//flowersSTAX.sortBySoil();
		outputXmlFile = "output.stax.xml";
		staxController.createXMLFile(outputXmlFile);
	}

}
