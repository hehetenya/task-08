package com.epam.rd.java.basic.task8;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private VisualParameters param;
    private GrowingTips tips;
    private String multiplying;

    public Flower(String name, String soil, String origin, VisualParameters param, GrowingTips tips, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.param = param;
        this.tips = tips;
        this.multiplying = multiplying;
    }

    public Flower() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setParam(VisualParameters param) {
        this.param = param;
    }

    public void setTips(GrowingTips tips) {
        this.tips = tips;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public VisualParameters getParam() {
        return param;
    }

    public GrowingTips getTips() {
        return tips;
    }

    public String getMultiplying() {
        return multiplying;
    }
}
