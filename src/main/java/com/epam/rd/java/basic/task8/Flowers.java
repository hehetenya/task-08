package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Flowers {

    private List<Flower> flowers;

    public Flowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    void sortByName(){
        flowers.sort(Comparator.comparing(Flower::getName));
    }
    void sortBySoil(){
        flowers.sort(Comparator.comparing(Flower::getSoil));
    }
    void sortByOrigin(){
        flowers.sort(Comparator.comparing(Flower::getOrigin));
    }

}
