package com.epam.rd.java.basic.task8;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private int aveLenFlower;

    public VisualParameters(String stemColour, String leafColour, int aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public VisualParameters() {
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }
}
